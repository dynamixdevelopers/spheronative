/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.spheronative;

import java.util.*;

import android.os.Handler;
import android.os.RemoteException;
import orbotix.robot.base.CollisionDetectedAsyncData;
import orbotix.robot.base.Robot;
import orbotix.robot.base.RobotProvider;
import orbotix.robot.sensor.AccelerometerData;
import orbotix.robot.sensor.AttitudeSensor;
import orbotix.robot.sensor.DeviceSensorsData;
import orbotix.robot.sensor.GyroData;
import orbotix.sphero.*;
import org.ambientdynamix.api.contextplugin.*;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import org.ambientdynamix.contextplugins.ambientcontrol.*;

/**
 * @author Darren Carlson
 */
public class SpheronativeRuntime extends ContextPluginRuntime {
    private static final int VALID_CONTEXT_DURATION = 60000;
    private static final double ACC_X_THRESHOLD = 2;
    // Static logging TAG
    private final String TAG = this.getClass().getSimpleName();
    // Our secure context
    private Context context;
    private Sphero mRobot;
    private ControlConnectionManager controlConnectionManager;

    private static final int PITCH_THRESHOLD = 10;
    private static final int ROLL_THRESHOLD = 10;
    private static final int YAW_THRESHOLD = 10;
    private Handler handler = new Handler();


    /**
     * Called once when the ContextPluginRuntime is first initialized. The implementing subclass should acquire the
     * resources necessary to run. If initialization is unsuccessful, the plug-ins should throw an exception and release
     * any acquired resources.
     */
    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        // Set the power scheme
        this.setPowerScheme(powerScheme);
        // Store our secure context
        this.context = this.getSecuredContext();
        controlConnectionManager = new ControlConnectionManager(this, clientListener, serverListener, new TranslatingProfileMatcher(), TAG);
        RobotProvider.getDefaultProvider().addConnectionListener(connectionListener);
        RobotProvider.getDefaultProvider().addDiscoveryListener(discoveryListener);
        connect();
    }

    /**
     * Called by the Dynamix Context Manager to start (or prepare to start) context sensing or acting operations.
     */
    @Override
    public void start() {
        try {
            getPluginFacade().getDynamixFacade(getSessionId()).openSession();
            connect();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Started!");
    }
    /**
     * Called by the Dynamix Context Manager to stop context sensing or acting operations; however, any acquired
     * resources should be maintained, since start may be called again.
     */
    @Override
    public void stop() {
        if (mRobot != null) {
            try {
                RobotProvider.getDefaultProvider().endDiscovery();
                getPluginFacade().getDynamixFacade(getSessionId()).closeSession();
                controlConnectionManager.stop();
                stopRobot();
            } catch (Exception e) {
                Log.w(TAG, "Error removing robot listeners: " + e.toString());
            }
            Log.d(TAG, "Stopped!");
        }
    }

    /**
     * Stops the runtime (if necessary) and then releases all acquired resources in preparation for garbage collection.
     * Once this method has been called, it may not be re-started and will be reclaimed by garbage collection sometime
     * in the indefinite future.
     */
    @Override
    public void destroy() {
        RobotProvider.getDefaultProvider().removeConnectionListeners();
        RobotProvider.getDefaultProvider().removeDiscoveryListeners();
        stop();
        if (mRobot != null) {
            mRobot.disconnect();
            mRobot = null;
            context = null;
        }
        RobotProvider.getDefaultProvider().disconnectControlledRobots();
        Log.d(TAG, "Destroyed!");
    }

    private ControlConnectionManager.IControllConectionManagerListener clientListener = new ControlConnectionManager.ControllConnectionManagerListenerAdapter() {
        private boolean listenerAdded;

        @Override
        public void controllRequest(CommandsEnum command, String sourcePluginId) {
            if (command.equals(CommandsEnum.DISPLAY_COLOR)) ;
            {
                sendDisplayFeedback = true;
                SensorControl control = mRobot.getSensorControl();
                control.enableStreaming(true);
                control.addSensorListener(sListener, SensorFlag.ACCELEROMETER_NORMALIZED);
                control.setRate(20);
                listenerAdded = true;
            }
        }

        @Override
        public void stopControlling(String contextType) {
            if (listenerAdded) {
                Log.i(TAG, "stopping controlling removing attitude listener");
                stopRobot();
                listenerAdded = false;
            }
        }

        @Override
        public void consumeIToggleCommand(IToggleCommand iToggleCommand, String sourcePluginId, UUID request) {
//            Log.i(TAG, "consuming 3DBase message");
            CommandsEnum command = iToggleCommand.getCommand();
            final float speed = 0.6f;
            switch (command) {
                case MOVEMENT_BACKWARD:
                    mRobot.drive(180f, speed);
                    return;
                case MOVEMENT_FORWARD:
                    mRobot.drive(0f, speed);
                    return;
                case MOVEMENT_LEFT:
                    mRobot.drive(270f, speed);
                    return;
                case MOVEMENT_RIGHT:
                    mRobot.drive(90f, speed);
                    return;
                case MOVEMENT_NEUTRAL:
                    mRobot.stop();
                    return;
                case MOVEMENT_FORWARD_LEFT:
                    mRobot.drive(315f, speed);
                    return;
                case MOVEMENT_FORWARD_RIGHT:
                    mRobot.drive(45f, speed);
                    return;
                case MOVEMENT_BACKWARD_LEFT:
                    mRobot.drive(225f, speed);
                    return;
                case MOVEMENT_BACKWARD_RIGHT:
                    mRobot.drive(135f, speed);
                    return;
            }
        }

        @Override
        public void consumePYRSensor(IPYRSensor ipyrSensor, String sourcePluginId, UUID request) {
//            mRobot.setColor(0, 255, 0);
//            Log.i(TAG,"consuming 3DPYR message");
            if (Math.abs(ipyrSensor.getRoll()) > ROLL_THRESHOLD || Math.abs(ipyrSensor.getPitch()) > PITCH_THRESHOLD) {
                float pitchNorm = (float) ipyrSensor.getPitch() / (float) Math.max(Math.abs(ipyrSensor.getPitch()), Math.abs(ipyrSensor.getRoll()));
                float rollNorm = (float) ipyrSensor.getRoll() / (float) Math.max(Math.abs(ipyrSensor.getPitch()), Math.abs(ipyrSensor.getRoll()));
                final float heading;
                final float speed;
                if (pitchNorm < 0) { //forward
                    if (rollNorm < 0) { //left 270 - 359
                        heading = (315f - pitchNorm * 45f + rollNorm * 45f);
                    } else { // right 0 - 90
                        heading = (45f + pitchNorm * 45f + rollNorm * 45f);
                    }
                } else { //backward
                    if (rollNorm < 0) { //left 180 - 270
                        heading = (225f - pitchNorm * 45f - rollNorm * 45f);
                    } else { // right 90 - 180
                        heading = (135 + pitchNorm * 45f - rollNorm * 45f);
                    }
                }
                speed = (Math.abs(ipyrSensor.getRoll()) + Math.abs(ipyrSensor.getPitch())) / 360f;
//                Log.i(TAG,"driving: " + heading + " at speed: " + speed);
                mRobot.drive(heading, speed * 2);
            }
        }
    };

    private boolean sendDisplayFeedback;
    Set<CommandsEnum> storedRequests = new HashSet<CommandsEnum>();
    private ControlConnectionManager.IControllConectionManagerListener serverListener = new ControlConnectionManager.ControllConnectionManagerListenerAdapter() {

        private boolean listenerAdded;

        @Override
        public void controllRequest(CommandsEnum command, String s) {
            if (mRobot == null) {
                connect();
                Log.w(TAG, "Robot not connected storing controll request for " + command);
                storedRequests.add(command);
//                controlConnectionManager.getControllChannels().clear();
            } else {
                mRobot.enableStabilization(false);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.i(TAG, "init control logic for " + command);
                if (command.equals(CommandsEnum.SENSOR_TOGGLE)) {
                    mRobot.getCollisionControl().startDetection(50, 0, 50, 0, 100);
                    mRobot.getCollisionControl().addCollisionListener(cListener);
                } else if (!listenerAdded && (command.equals(CommandsEnum.SENSOR_PYR) ||
                        command.equals(CommandsEnum.SENSOR_ACC) ||
                        command.equals(CommandsEnum.SENSOR_GYRO))) {
                    SensorControl control = mRobot.getSensorControl();
                    control.enableStreaming(true);
                    control.addSensorListener(sListener, SensorFlag.ACCELEROMETER_NORMALIZED, SensorFlag.GYRO_NORMALIZED, SensorFlag.ATTITUDE);
                    control.setRate(50);
                    listenerAdded = true;
                }
                mRobot.setColor(0, 0, 255);
            }


        }

        @Override
        public void stopControlling(String s) {
            if (listenerAdded) {
                Log.i(TAG, "stopping controlling removing attitude listener");
                stopRobot();
                listenerAdded = false;
                storedRequests.clear();
            }

        }

        @Override
        public void consumeDisplayCommand(IDisplayCommand iDisplayCommand, String sourcePluginId, UUID request) {
            Log.i(TAG, "received display command: " + iDisplayCommand.getR() + "'" + iDisplayCommand.getG() + "'" + iDisplayCommand.getB());
            if (iDisplayCommand.getCommand().equals(CommandsEnum.DISPLAY_COLOR)) {
//                Log.i(TAG,"setting color: " + iDisplayCommand.getR() + "'" + iDisplayCommand.getG() + "'" + iDisplayCommand.getB());
                mRobot.setColor(iDisplayCommand.getR(), iDisplayCommand.getG(), iDisplayCommand.getB());
            }
        }
    };

    @Override
    public void onMessageReceived(Message message) {
        try {
            controlConnectionManager.handleConfigCommand(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
        return controlConnectionManager.addContextListener(listenerInfo);
    }



    private void stopRobot() {
        if (mRobot != null) {
            mRobot.getSensorControl().removeSensorListener(sListener);
            mRobot.getCollisionControl().removeCollisionListener(cListener);
            mRobot.stop();
            mRobot.setColor(255, 0, 0);
            mRobot.enableStabilization(true);
            mRobot.getSensorControl().stopStreaming();
            mRobot.getCollisionControl().stopDetection();
        }
    }



    private ConnectionListener connectionListener = new ConnectionListener() {
        @Override
        public void onConnected(Robot robot) {
            mRobot = (Sphero) robot;
            connected();
        }

        @Override
        public void onConnectionFailed(Robot sphero) {
            Log.i(TAG, "Connection Failed: " + sphero + " retrying");
            stopRobot();
            RobotProvider.getDefaultProvider().endDiscovery();
            RobotProvider.getDefaultProvider().startDiscovery(context);

        }

        @Override
        public void onDisconnected(Robot robot) {
            Log.d(TAG, "Disconnected: " + robot);
            stopRobot();
            mRobot = null;
        }
    };

    private DiscoveryListener discoveryListener = new DiscoveryListener() {
        @Override
        public void onBluetoothDisabled() {
            Log.d(TAG, "Bluetooth Disabled");
        }

        @Override
        public void discoveryComplete(List<Sphero> spheros) {
            Log.d(TAG, "Found " + spheros.size() + " robots");
        }

        @Override
        public void onFound(List<Sphero> sphero) {
            Log.d(TAG, "Found: " + sphero);
            RobotProvider.getDefaultProvider().connect(sphero.iterator().next());
        }
    };

    private void connect() {
        if (mRobot != null)  //already connected
            return;
        try {
            boolean success = RobotProvider.getDefaultProvider().startDiscovery(context);
            if (!success) {
                Log.d(TAG, "Connection Failed: ");
            }
        } catch (Exception e) {
            Log.e(TAG, "Sphero init error: " + e);
            e.printStackTrace();
        }
    }

    private SensorListener sListener = new SensorListener() {
        @Override
        public void sensorUpdated(DeviceSensorsData sensorDataArray) {
//            Log.d(TAG, sensorDataArray.toString());

            sendPYRSensorData(sensorDataArray);
            sendACCSensorData(sensorDataArray);
            sendGyroData(sensorDataArray);
        }
    };

    private void sendGyroData(DeviceSensorsData sensorDataArray) {
        GyroData gyroData = sensorDataArray.getGyroData();
        controlConnectionManager.sendControllCommand(new GyroSensor(gyroData.getRotationRateFiltered().x,
                gyroData.getRotationRateFiltered().y,
                gyroData.getRotationRateFiltered().z), CommandsEnum.SENSOR_GYRO);
    }

    private CollisionListener cListener = new CollisionListener() {

        public void collisionDetected(CollisionDetectedAsyncData collisionData) {
            Log.d(TAG, collisionData.toString());
            controlConnectionManager.sendControllCommand(new ToggleSensor(), CommandsEnum.SENSOR_TOGGLE);
        }
    };

    private void sendACCSensorData(DeviceSensorsData data) {
        AccelerometerData accelerometerData = data.getAccelerometerData();
        controlConnectionManager.sendControllCommand(new ACCSensor(accelerometerData.getFilteredAcceleration().x,
                accelerometerData.getFilteredAcceleration().y,
                accelerometerData.getFilteredAcceleration().z), CommandsEnum.SENSOR_ACC);
    }

    private void sendPYRSensorData(DeviceSensorsData data) {
        AttitudeSensor attitudeData = data.getAttitudeData();
        controlConnectionManager.sendControllCommand(new PYRSensor(attitudeData.pitch, attitudeData.yaw, attitudeData.roll), CommandsEnum.SENSOR_PYR);
    }

    private void connected() {
        Log.d(TAG, "Connected On Thread: " + Thread.currentThread().getName());
        Log.d(TAG, "Connected: " + mRobot);

        mRobot.enableStabilization(true);
        mRobot.drive(90, 0);
        mRobot.setBackLEDBrightness(.8f);

        mRobot.setColor(255, 0, 0);


        boolean preventSleepInCharger = mRobot.getConfiguration().isPersistentFlagEnabled(PersistentOptionFlags.PreventSleepInCharger);
        Log.d(TAG, "Prevent Sleep in charger = " + preventSleepInCharger);
        Log.d(TAG, "VectorDrive = " + mRobot.getConfiguration().isPersistentFlagEnabled(PersistentOptionFlags.EnableVectorDrive));

        mRobot.getConfiguration().setPersistentFlag(PersistentOptionFlags.PreventSleepInCharger, false);
        mRobot.getConfiguration().setPersistentFlag(PersistentOptionFlags.EnableVectorDrive, true);

        Log.d(TAG, "VectorDrive = " + mRobot.getConfiguration().isPersistentFlagEnabled(PersistentOptionFlags.EnableVectorDrive));
        Log.v(TAG, mRobot.getConfiguration().toString());
        for (CommandsEnum storedRequest : storedRequests) {
            serverListener.controllRequest(storedRequest, "");
        }
//        storedRequests.clear();
    }

    boolean blinking = true;

    private void stopBlink() {
        blinking = false;
    }

    /**
     * Causes the robot to blink once every second.
     *
     * @param lit
     */
    private void blink(final boolean lit) {
        if (mRobot == null) {
            blinking = false;
            return;
        }

        //If not lit, send command to show blue light, or else, send command to show no light
        if (lit) {
            mRobot.setColor(0, 0, 0);

        } else {
            mRobot.setColor(255, 0, 0);
        }

        if (blinking) {
            //Send delayed message on a handler to run blink again

            handler.postDelayed(new Runnable() {
                public void run() {
                    blink(!lit);
                }
            }, 2000);
        }
    }


    private void move() {
        mRobot.drive(90.0f, 0.5f);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mRobot.drive(90.0f, 0.0f);
    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {
//        Log.i(TAG, "Received configured context request");
        controlConnectionManager.handleConfiguredContextRequest(requestId, contextType, config);
    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {
        // Not supported
    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {
        // Not supported
    }
}