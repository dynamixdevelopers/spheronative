# The Sphero Plugin
The Sphero Plugin allows the user to control an Orbotix robot or receive sensor data from it.
The plugin supports Sphero version 1,2 and the Ollie.

All interaction is performed through the Ambient Control mechanisms. See [Ambient Control documentation](https://bitbucket.org/dynamixdevelopers/ambientcontrolplugin)

The plugins control description is as follows:
```JSON
{
    "name":"SpheroPlugin",
    "artifact_id":"org.ambientdynamix.contextplugins.spheronative",
    "description":"",
    "pluginVersion":"1.0.0",
    "owner_id":"",
    "platform":"",
    "minPlatformVersion":"",
    "minFrameworkVersion":"",
    "inputList": [ 
        {
            "mandatoryControls":["SENSOR_PYR"],
            "priority":1
        },
        {
            "mandatoryControls":["SENSOR_AXIS"],
            "priority":2
        },
        {
            "mandatoryControls":[
                "MOVEMENT_FORWARD_LEFT",
                "MOVEMENT_BACKWARD_LEFT",
                "MOVEMENT_BACKWARD",
                "MOVEMENT_LEFT",
                "MOVEMENT_RIGHT",
                "MOVEMENT_FORWARD",
                "MOVEMENT_FORWARD_RIGHT",
                "MOVEMENT_BACKWARD_RIGHT"
            ],
            "priority":3
        },
        {
            "mandatoryControls":["DISPLAY_COLOR"],
            "priority":4
        },
        {
            "mandatoryControls":["SWITCH"],
            "priority":5
        }
    ],
    "optionalInputList":["DISPLAY_COLOR","SWITCH"],
    "outputList":{
        "Accelerometer":"SENSOR_ACC",
        "Collision":"SENSOR_TOGGLE",
        "Pitch Yaw Roll":"SENSOR_PYR",
        "Gyroscope":"SENSOR_GYRO"
    }    
}

```