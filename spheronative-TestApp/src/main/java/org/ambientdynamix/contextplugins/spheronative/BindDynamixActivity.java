package org.ambientdynamix.contextplugins.spheronative;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import java.util.Arrays;
import java.util.List;
import org.ambientdynamix.api.application.*;
import org.ambientdynamix.contextplugins.spheronative.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by workshop on 11/4/13.
 */
public class BindDynamixActivity extends Activity {

    private final String TAG = this.getClass().getSimpleName();

    private ContextHandler contextHandler;
    private DynamixFacade dynamix;
    private PluginInvoker pluginInvoker;

    /**
     * Called when the activity is first created.
     *
     * @param savedInstanceState If the activity is being re-initialized after
     *                           previously being shut down then this Bundle contains the data it most
     *                           recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pluginInvoker = new PluginInvoker();

        if (PluginInvoker.AUTOMATIC_EXECUTION) {
            setContentView(R.layout.main_auto);
            connect();
        } else {
            setContentView(R.layout.main);

            Button btnConnect = (Button) findViewById(R.id.btnConnect);
            btnConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    connect();
                }
            });

            // Setup the disconnect button
            Button btnDisconnect = (Button) findViewById(R.id.btnDisconnect);
            btnDisconnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disconnect();
                }
            });

         /*
         * Setup the interactive context acquisition button. Note that this method only works if the
		 * 'org.ambientdynamix.sampleplugin' plug-in is installed.
		 */
            Button btnInvokePlugin = (Button) findViewById(R.id.invokePlugin);
            btnInvokePlugin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invokePlugins();
                }


            });
        }
    }

    private void invokePlugins() {
        /**
         * This gets called once the user presses the invoke plugins button on the device. The deafult behaviour is to
         * request all context Types listed in the cotextRequestType Array
         */
        if (dynamix != null) {
            try {
                Log.i(TAG, "A1 - Requesting Programmatic Context Acquisitions");
                for (PluginInvoker.PluginInvocation pluginInvocation : pluginInvoker.getPluginInvocations()) {
                    if (pluginInvocation.getConfiguration() != null) {
                        contextHandler.contextRequest(pluginInvocation.getPluginId(),
                                pluginInvocation.getContextRequestType(), pluginInvocation.getConfiguration(), new IContextRequestCallback.Stub() {
                            @Override
                            public void onSuccess(ContextResult contextEvent) throws RemoteException {
                                Log.i(TAG, "A1 - Request id was: " + contextEvent.getResponseId());
                                contextListener.onContextResult(contextEvent);
                            }

                            @Override
                            public void onFailure(String s, int i) throws RemoteException {
                                Log.w(TAG, "Call was unsuccessful! Message: " + s + " | Error code: "
                                        + i);
                            }
                        });
                    } else
                        contextHandler.contextRequest(pluginInvocation.getPluginId(),
                                pluginInvocation.getContextRequestType(), new IContextRequestCallback.Stub() {
                            @Override
                            public void onSuccess(ContextResult contextEvent) throws RemoteException {
                                Log.i(TAG, "A1 - Request id was: " + contextEvent.getResponseId());
                                contextListener.onContextResult(contextEvent);
                            }

                            @Override
                            public void onFailure(String s, int i) throws RemoteException {
                                Log.w(TAG, "Call was unsuccessful! Message: " + s + " | Error code: "
                                        + i);
                            }
                        });
                }
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        } else
            Log.w(TAG, "Dynamix not connected.");
    }

    private void disconnect() {
        if (contextHandler != null) {
            try {
                /*
                 * In this example, this Activity controls the session, so we call closeSession here. This will
                 * close the session for ALL of the application's IDynamixListeners.
                 */
                dynamix.closeSession(new Callback() {
                    @Override
                    public void onFailure(String message, int errorCode) throws RemoteException {
                        Log.w(TAG, "Call was unsuccessful! Message: " + message + " | Error code: "
                                + errorCode);
                    }

                    @Override
                    public void onSuccess() throws RemoteException {
                        Log.w(TAG, "Session closed");
                    }
                });

            } catch (RemoteException e) {
                Log.e(TAG, e.toString());
            }
        }
    }

    private void connect() {
        if (contextHandler == null) {
            try {
                DynamixConnector.openConnection(this, true, null, new ISessionCallback.Stub() {
                    @Override
                    public void onSuccess(DynamixFacade iDynamixFacade) throws RemoteException {
                        dynamix = iDynamixFacade;
                        dynamix.createContextHandler(new ContextHandlerCallback() {
                            @Override
                            public void onSuccess(ContextHandler handler) throws RemoteException {
                                contextHandler = handler;
                                registerForContextTypes();
                            }

                            @Override
                            public void onFailure(String message, int errorCode) throws RemoteException {
                                super.onFailure(message, errorCode);
                            }
                        });//

                    }

                    @Override
                    public void onFailure(String s, int i) throws RemoteException {

                    }
                });
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /*
     * The ServiceConnection is used to receive callbacks from Android telling our application that it's been connected
	 * to Dynamix, or that it's been disconnected from Dynamix. These events come from Android, not Dynamix. Dynamix
	 * events are always sent to our IDynamixListener object (defined farther below), which is registered (in this case)
	 * in during the 'addDynamixListener' call in the 'onServiceConnected' method of the ServiceConnection.
	 */


    private ContextListener contextListener = new ContextListener() {


        @Override
        public void onContextResult(ContextResult event) throws RemoteException {
            /*
             * Log some information about the incoming event
			 */
            Log.i(TAG, "A1 - onContextEvent received from plugin: " + event.getResultSource());
            Log.i(TAG, "A1 - -------------------");
            Log.i(TAG, "A1 - Event context type: " + event.getContextType());
            Log.i(TAG, "A1 - Event timestamp " + event.getTimeStamp().toLocaleString());
            if (event.expires())
                Log.i(TAG, "A1 - Event expires at " + event.getExpireTime().toLocaleString());
            else
                Log.i(TAG, "A1 - Event does not expire");
			/*
			 * To illustrate how string-based context representations are accessed, we log each contained in the event.
			 */
            for (String format : event.getStringRepresentationFormats()) {
                Log.i(TAG,
                        "Event string-based format: " + format + " contained data: "
                                + event.getStringRepresentation(format));
            }
            boolean done = true;
            for (PluginInvoker.PluginInvocation pluginInvocation : pluginInvoker.getPluginInvocations()) {
                if (pluginInvocation.getContextRequestType().equals(event.getContextType())) {
                    pluginInvocation.setSuccessfullyCalled(true);
                    pluginInvoker.invokeOnResponse(event);

                }
                if (!pluginInvocation.isSuccessfullyCalled())
                    done = false;
            }

            if (done && PluginInvoker.AUTOMATIC_EXECUTION) {
                disconnect();
            }
        }
    };

    /*
	 * Utility method that registers for the context types needed by this class.
	 */
    private void registerForContextTypes() throws RemoteException {
		/*
		 * Below are several examples of adding context support using context types. In this case, the plug-in (or
		 * plug-ins) assigned to handle context type will be automatically selected by Dynamix.
		 */
        for (PluginInvoker.PluginInvocation pluginInvocation : pluginInvoker.getPluginInvocations()) {
            String type = pluginInvocation.getContextRequestType();
            contextHandler.addContextSupport(pluginInvocation.getPluginId(), type, new ContextSupportCallback(){
                @Override
                public void onFailure(String message, int errorCode) throws RemoteException {
                    Log.w(TAG,
                            "Call was unsuccessful! Message: " + message + " | Error code: "
                                    + errorCode);
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "ON DESTROY for: Dynamix Simple Logger (A1)");
		/*
		 * Always remove our listener and unbind so we don't leak our service connection
		 */
        if (contextHandler != null) {
            try {
                dynamix.closeSession();
                contextHandler = null;
            } catch (RemoteException e) {
            }
        }
        super.onDestroy();
    }
}









